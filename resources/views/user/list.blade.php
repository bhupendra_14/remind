@extends('layout')

@section('content')
<div class="content" style="padding-top: 10px;">
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box ribbon-box">
                    <div class="ribbon ribbon-blue float-left" style="font-size:16px;"><i class=" fas fa-volleyball-ball  fa-spin"></i>&nbsp; <span>Reminder</span>
                        <a style="padding: 0.0rem 0.8rem;" href="{{ route('add_reminder') }}" class=" btn-roundeds btn btn-warning btn-sm waves-effect waves-light">
                            <i class="fa fa-plus"></i></a>
                    </div>
                    <h5 class="text-blue float-right" style="margin: 8px 0;">
                    </h5>
                    <div class="ribbon-content">
                        <div class="row">

                            <div class="col-lg-12">
								<div class="alertDiv">
                                	@if ($message = Session::get('success'))
									<div class="alert alert-success">
										<p>{{ $message }}</p>
									</div>
									@endif
								</div>
                                <table data-toggle="table" data-search="true" data-show-refresh="false" data-show-columns="true" data-page-list="[10,25,50]" data-page-size="10" data-pagination="true" data-show-pagination-switch="true" class="table-borderless">
                                    <thead class="thead-light">
                                        <tr>
                                            <th data-sortable="true">Title</th>
                                            <th data-sortable="true">Description</th>
                                            <th data-sortable="true">Date and Time</th>
                                            <th data-sortable="true">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if (!empty($reminds) && count($reminds) > 0)
                                        @foreach ($reminds as $remind)
                                        <tr>
                                            <td>{{ $remind->title }}</td>
                                            <td>{{ $remind->description }}</td>
                                            <td>{{ ($remind->datetime)  }}</td>
                                            <td>
                                                <a href="{{ route('edit', $remind->id) }}" class="btn-roundeds btn btn-warning btn-sm waves-effect waves-light">
                                                    Edit</a>
                                                <button data-id="{{$remind->id }}" id="delete" class="btn-roundeds btn btn-warning btn-sm waves-effect waves-light">
                                                    Delete</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- end row -->
    </div> <!-- container -->

</div>


@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $(document).on("click", "#delete", function() {
			var site_url = "{{ url('/') }}";
            if (confirm("Are you sure want to delete?")) {
                try {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: site_url + "/delete-reminder/"+$(this).attr("data-id")
                        , type: 'GET'
                        , datatype: "application/json"
                        , success: function(data) {
							console.log(data);
                            if (data != '') {
                                data = JSON.parse(data);
                                if (data.status_code == 200 && data.message != '') {
                                    $(".alertDiv").html('<div class="alert alert-success"><p>'+data.message+'</p></div>');
                                    setTimeout(function() {
                                        window.location = data.data
                                            .redirect;
                                    }, 2000);
                                } else {
                                    window.location = data.data.redirect;
                                }
                            }
                        },
                        // complete:function(){ },
                        error: function(jqXHR, exception) {
                            window.location = site_url;
                        }
                    , });
                } catch (e) {
                    console.log(e);
                }
            }
        });
    });

</script>
@endpush

