@extends('layout')

@section('content')

	<div class="content" style="padding-top: 10px;">
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box ribbon-box">
                        <div class="ribbon ribbon-blue float-left" style="font-size:16px;"><i class=" fas fa-volleyball-ball  fa-spin"></i>&nbsp; 		<span>Add Reminder</span>
                            <a style="padding: 0.0rem 0.8rem;" href="{{ route('home') }}" class=" btn-roundeds btn btn-warning btn-sm waves-effect waves-light">
                                Back</a>	
                        </div>
                        <h5 class="text-blue float-right" style="margin: 8px 0;">
                        </h5>
                        <div class="ribbon-content">
                          	 <div class="row"></br>
	                            <div class="col-lg-8 offset-2">
	                            	@if(Session::has('success'))
									<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
									@endif
                           			<form method="POST" action="{{ route('insert_reminder') }}">
										@csrf
                                        <div class="form-group mb-3">
                                            <label for="title">Title <span>*</span></label>
                                            <input class="form-control" name="title" type="text" placeholder="Enter your Title">
                                            @error('title')
                                                <label>{{$message}}<label>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="description">Description <span>*</span></label>
                                            <textarea rows="5" class="form-control" name="description" type="text" placeholder="Enter your Description"></textarea>
                                            @error('description')
                                                <label>{{$message}}<label>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="datetime">Select Date Time <span>*</span></label>
                                            <input id="datetime-datepicker" class="form-control flatpickr-input" name="datetime" type="text" readonly="readonly">
                                            @error('datetime')
                                                <label>{{$message}}<label>
                                            @enderror
                                        </div>
                                        
                                        <div class="form-group text-center mt-4 mb-3">
                                        	<button name="submit" type="submit" class="btn btn-rounded btn-dark waves-effect waves-light">Save </button>
                                    	</div>
                                    </form>
	                            </div>
	                        </div>
                        </div>
                    </div>
                </div>

               
            </div>
            <!-- end row -->
		</div> <!-- container -->

    </div>
@endsection

