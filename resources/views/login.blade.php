<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8" />
        <title>Practical</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App css -->
        <link href="{{ asset('public/main_assets/css/bootstrap.min.css') }}" media="screen" rel="stylesheet">
        <link href="{{ asset('public/main_assets/css/icons.min.css') }}" media="screen" rel="stylesheet">
        <link href="{{ asset('public/main_assets/css/app.min.css') }}" media="screen" rel="stylesheet">
    </head>
    <body class="" style="background-color:#F0DCA9;">
        <div class="account-pages mt-5 mb-2">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card" style="background-color:#C09F42;border: solid white 8px;">
                            <div class="card-body p-4">
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                               <form action="{{route('submit_login')}}"  method="post">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <label for="email" class="text-white">Email <span style="display:unset;color:#464545;">*</span></label>
                                        <input class="form-control" name="email" type="text" placeholder="Enter your email" value="{{ old('email') }}">
                                        @error('email')
                                            <label class="text-white">{{$message}}<label>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="password" class="text-white">Password <span style="display:unset;color:#464545;">*</span></label>
                                        <input class="form-control" name="password" type="password" id="password" placeholder="Enter your password">
                                        @error('password')
                                            <label class="text-white">{{$message}}<label>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-4 text-center">
                                        <button style="background-color:#F0DCA9;color:#201C15;" class="btn btn-block" type="submit"> Sign In </button>
                                    </div>
                                    <footer class="footer footer-alt">
                                        <p class="text-white">Don't have an account? <a href="{{route('registration')}}" class="text-white ms-1"><b>Sign Up</b></a></p>
                                    </footer>
								</form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
        <!-- Vendor js -->
        <script src="{{ asset('public/main_assets/js/vendor.min.js') }}"></script>
        <script src="{{ asset('public/main_assets/js/app.min.js') }}" ></script>
    </body>
</html>