<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8" />
        <title>Practical Test</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App css -->
		<link href="{{ asset('public/main_assets/css/bootstrap.min.css') }}" media="screen" rel="stylesheet">
		<link href="{{ asset('public/main_assets/css/icons.min.css') }}" media="screen" rel="stylesheet">
		<link href="{{ asset('public/main_assets/css/app.min.css') }}" media="screen" rel="stylesheet">
		<!-- Bootstrap Tables css -->
		<link href="{{ asset('public/main_assets/libs/bootstrap-table/bootstrap-table.min.css') }}" media="screen" rel="stylesheet">

        <link href="{{ asset('public/main_assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/main_assets/libs/clockpicker/bootstrap-clockpicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/main_assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
       
    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">
						<!-- Topbar Start -->
            <div class="navbar-custom" style="background-color:#C09F42">
                <ul class="list-unstyled topnav-menu float-right mb-0">
					<li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                          
                            <img src="{{ asset('public/main_assets/images/user.png'); }}" alt="user-image" class="rounded-circle">
                            <span class="pro-user-name ml-1">
                                <?php // echo $master[0]->user_name; ?>
                                <i class="mdi mdi-chevron-down"></i> 
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                           
                           
                            <!-- item-->
                            <a href="{{route('logout')}}" class="dropdown-item notify-item">
                                <i class="fe-log-out"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>
				</ul>
            
            <!-- LOGO -->
            <div class="logo-box">
                <a href="" class="logo text-center">
                    <span class="logo-lg">
                        <h3 class="mt-3 text-light">{{ Auth::user()->name  }}</h3>
                    </span>
                    <span class="logo-sm">
                        <h2 class="mt-3 text-light">{{ Str::substr(Auth::user()->name,0,1)  }}</h2>
                    </span>
                </a>
            </div>
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect waves-light">
                        <i class="fe-menu"></i>
                    </button>
                </li>
    
            </ul> 
            </div>
            <!-- end Topbar -->
			
						<!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">
							<li>
                                <a href="{{route('home')}}">
									<i class="fe-airplay"></i> <span> Reminder </span> 
								
								 </a>
							</li>
                          
						</ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
			<div class="content-page">
				<div class="">
					@yield('content')
				</div>	
				<!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                               
                            </div>
                            <div class="col-md-6">
                               <!-- end Footer <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript:void(0);">About Us</a>
                                    <a href="javascript:void(0);">Help</a>
                                    <a href="javascript:void(0);">Contact Us</a>
                                </div>--> 
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->
			</div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
		 <!-- Vendor js -->
     
		 <script src="{{ url('/public/main_assets/js/vendor.min.js') }}"></script>
		 <script src="{{ url('/public/main_assets/libs/bootstrap-table/bootstrap-table.min.js') }}" ></script>
		 <script src="{{ url('/public/main_assets/js/pages/bootstrap-tables.init.js') }}" ></script>
		 <script src="{{ url('/public/main_assets/js/app.min.js') }}" ></script>
        <!-- Plugins js-->
        <script src="{{ url('/public/main_assets/libs/flatpickr/flatpickr.min.js') }}"></script>
       
        <script src="{{ url('/public/main_assets/libs/clockpicker/bootstrap-clockpicker.min.js') }}"></script>
        <script src="{{ url('/public/main_assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
        
       <!-- Init js-->
       <script src="{{ url('/public/main_assets/js/pages/form-pickers.init.js') }}"></script>
		<script>
			$(document).ready(function(){
			  $('[data-toggle="tooltip"]').tooltip();
			});
		</script>
        @stack('scripts')
        
    </body>
</html>