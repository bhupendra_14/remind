<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{
    public function index()
    {        
      
        return view('registration');
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required' , 'string', 'max:155'],
            'email' => ['required' , 'string', 'max:155', 'email', 'unique:users'],
            'password' => ['required' , 'string', 'max:155'],
        ]);
       
        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            
        ]);
        return redirect()->route('registration')->with('success','User has been Register successfully. Now You can Sign In');
    }

  }
