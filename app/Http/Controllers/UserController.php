<?php

namespace App\Http\Controllers;

date_default_timezone_set('Asia/Kolkata');
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Remind;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Jobs\Reminder;
class UserController extends Controller
{
    public function index()
    {        
        $userid=Auth::id();
        $result = Remind::where([
            ['user_id',$userid]
         ])->get();
        
         
        return view('user.list')->with(['reminds' => $result]);
    }

    public function add_reminder()
    {        
        return view('user.add');
    }

    public function insert_reminder(Request $request)
    {
        $request->validate([
            'title' => ['required' , 'string', 'max:155'],
            'description' => ['required' , 'string'],
            'datetime' => ['required' , 'string', 'max:155'],
        ]);
        
        Remind::create([
            'user_id'=>Auth::id(),
            'title'=>$request->title,
            'description'=>$request->description,
            'datetime'=>$request->datetime,
          
        ]);
        $date =$request->datetime;
        $seconds = Carbon::now()->diffInSeconds(Carbon::parse($date), false);
        if($seconds <= 0){
            $seconds = 0;
        }
        dispatch(new Reminder(['email' => \Auth::user()->email,'title'=>$request->title,'description'=>$request->description]))->delay($seconds);
        return redirect()->route('home')->with('success','Reminder has been created successfully.');
    }

    public function edit($id)
    {
        $result = Remind::findOrFail($id);
        return view('user.edit')->with(['remind' => $result]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => ['required' , 'string', 'max:155'],
            'description' => ['required' , 'string'],
            'datetime' => ['required' , 'string', 'max:155'],
        ]);
        
        $updateArr = [
            'title'=>$request->title,
            'description'=>$request->description,
            'datetime'=>$request->datetime,
        ];   
        
        
        $date =$request->datetime;
        $seconds = Carbon::now()->diffInSeconds(Carbon::parse($date), false);
        if($seconds <= 0){
            $seconds = 0;
        }
        dispatch(new Reminder(['email' => \Auth::user()->email,'title'=>$request->title,'description'=>$request->description]))->delay($seconds);
        Remind::where('id', $request->remind_id)->update($updateArr);

        return redirect()->route('home')->with('success','User Has Been updated successfully');
        
    }

    public function delete($id)
    {
        Remind::where('id',$id)->delete();
        $redirect = route('home');
        $arr = array("redirect" => $redirect);
        $this->_json(200, 'Reminder deleted successfully', $arr);
    }

    function _json($code, $msg = "", $data = array())
    {
        @http_response_code($code);
        $result['status_code'] = $code;
        $result['message'] = $msg;
        $result['data'] = $data;
        echo json_encode($result);
        die;
    }
}
