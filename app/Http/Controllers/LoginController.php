<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Foundation\Auth\ThrottlesLogins;

use Route;
use session;
class LoginController extends Controller
{
    // use ThrottlesLogins;
    /**
     * Show the login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

    public function index()
    {
        return view('login');
    }
    
    /**
     * Login the admin.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
       $this->validator($request);

       if(Auth::attempt($request->only('email','password')))
       {
            
        //Authentication passed...
        return redirect()
        ->intended(route('home'))
        ->with('status',"You are logged in successfully.");
        }
        //Authentication failed...
        return $this->loginFailed();
    }
    /**
     * Logout.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
      Auth::logout();
      return redirect()
      ->route('login');
    }
    /**
     * Validate the form data.
     *
     * @param \Illuminate\Http\Request $request
     * @return
     */
    private function validator(Request $request)
    {
      //validation rules.
        $rules = [
            'email'    => 'required|email|exists:users|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];
    //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match with our records.',
        ];
    //validate the request.
        $request->validate($rules,$messages);
    }
    /**
     * Redirect back after a failed login.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginFailed()
    {
      return redirect()
        ->back()
        ->withInput()
        ->with('error','Login failed, please try again!');
    }

    public function username(){
        return 'email';
    }
}
