<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('Auth')->group(function(){
    Route::get('/', [App\Http\Controllers\LoginController::class, 'index'])->name('login');
    Route::post('/login', [App\Http\Controllers\LoginController::class, 'login'])->name('submit_login');
    Route::get('/logout', [App\Http\Controllers\LoginController::class, 'logout'])->name('logout');
});
Route::get('/registration', [App\Http\Controllers\RegistrationController::class, 'index'])->name('registration');
Route::post('/registration', [App\Http\Controllers\RegistrationController::class, 'register'])->name('register_user');


Route::get('/home', [App\Http\Controllers\UserController::class, 'index'])->name('home')->middleware('auth');
Route::get('/add-reminder', [App\Http\Controllers\UserController::class, 'add_reminder'])->name('add_reminder')->middleware('auth');
Route::post('/insert-reminder', [App\Http\Controllers\UserController::class, 'insert_reminder'])->name('insert_reminder')->middleware('auth');
Route::get('/edit-reminder/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('edit')->middleware('auth');
Route::post('/update-reminder', [App\Http\Controllers\UserController::class, 'update'])->name('update_reminder')->middleware('auth');
Route::get('/delete-reminder/{id}', [App\Http\Controllers\UserController::class, 'delete'])->name('delete')->middleware('auth');



